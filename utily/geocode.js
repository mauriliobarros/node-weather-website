const request = require('request')

const geocode = (address,callback) =>
{
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+ encodeURIComponent(address) +'.json?access_token=pk.eyJ1IjoibWF4aGVyZSIsImEiOiJja2t6dXprbW4zMzN1MnBtcjI4cTdxOXNxIn0.Nm1-c-t4h0Vm0hzi35rOGw&limit=1'

    request ({ url: url, json: true}, (error, {body}) => 
    {
        if (error) 
        {
            callback ('Unable to connect to location services!', undefined)
        } else if (body.error === [0]) 
        {
            callback('Unable to find location. try another search', undefined)
        } else
        {
            callback(undefined, {
                latitude: body.features[0].center[1],
                longitude: body.features[0].center[0],
                place: body.features[0].place_name
            })
        }

    })
}

module.exports = (geocode)