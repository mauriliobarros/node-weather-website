const path = require ('path')
const express = require ('express')
const hbs = require ('hbs')
const geocode = require('../utily/geocode')
const forecast = require('../utily/forecast')
const port = process.env.PORT || 3000
//console.log(__dirname)
//console.log(path.join(__filename))

// Usando a Biblioteca Express para caminhos 
const app = express()
const publicDirectoryPath = path.join(__dirname, '../public')
const templatePath = path.join(__dirname,'../template/views')
const partialsPath = path.join(__dirname,'../template/partials')


//Usando o handlebars ou hbs and views location
app.set('view engine', 'hbs')
app.set('views', templatePath)
hbs.registerPartials(partialsPath)

//Setup static directory to serve
app.use(express.static(publicDirectoryPath))

app.get('',(req, res) =>
{
    res.render('index',
    {
        title:'Weather App',
        name:'Maurilio'
    })
})
app.get('/weather',(req,res)=>
{
    address = req.query.address
    if(!req.query.address)
    {
        return res.send 
        ({
            erro: 'You must provide a address term to Weather'
        })
    } else
    {
        geocode(address, (error,{latitude,longitude,place}={}) =>
        {
            if(error)
            {
                return res.send
                ({
                    errorMessage: 'Error:', error
                })
            }
            
            forecast(latitude, longitude, (error, dataf) => 
            {
                if(error)
                {
                    return res.send
                    ({
                        errorMessage: 'Error:', error
                    })
                }
                res.send
                ({
                    Localizacao: place,
                    Previsao_Tempo: dataf
                })
            })
            
        })
    }
    /*res.send
    ({
        forecast: 'it is snowing',
        geocode: 'philadelphia',
        address: req.query.address
    })*/
})

app.get('/products',(req, res)=>
{
    if(!req.query.search)
    {
        return res.send 
        ({
            error: 'You must provide a search term'
        })
    }
    console.log(req.query.search)
    res.send
    ({
        products: []
    })
})

app.get('/about',(req, res) =>
{
    res.render('about',
    {
        title:'About page',
        name:'Maurilio B.M.'
    })
})

app.get('/help',(req, res)=>
{
    res.render('help',
    {
        title:'Help Page',
        name:'call 190 for help'
    })
})

app.get('',(req, res) =>
{
    res.send('Hello Express!')
})
//

app.get('/help/*',(req, res) =>
{
    res.render('404',
    {
        title:'404 Article', 
        name: 'Volte para uma pagina existente.',
        errorMessage: 'Help - Artigo não Existe!'
    })
})

app.get('/about/*',(req, res) =>
{
    res.render('404',
    {
        title:'404 Article', 
        name: 'Volte para uma pagina existente.',
        errorMessage: 'About - Artigo não Existe!'
    })
})

app.get('/weather',(req, res) =>
{
    //res.send ('<p></p>')
    res.send
    (['<h1>Informacoes para o Geocode</h1>',{
        latitude: 40,
        longitude: -70,               
    },
    {
        local: 'Cafundo do judas'
    }])    
})

app.get('*',(req,res)=>
{
    res.render('404',
    {
        title:'Pagina 404 essa pagina que está tentando acessar não existe', 
        name: 'Volte para uma pagina existente.',
        errorMessage: 'Pagina não Existe!'
    })


})

app.listen(port,()=>
{
    console.log('Server is up on port.'+port)
})